#!/bin/bash
C=$(printf '\033')
RED="${C}[1;31m"
GREEN="${C}[1;32m"
NC="${C}[0m"

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

sudo timedatectl set-local-rtc 1 --adjust-system-clock

# OR if the prev one fails do this
# sudo dpkg-reconfigure tzdata
# sudo ntpd -qg

# If the above fails then use this

echo "${GREEN}[*]Is it worked"
echo "yes -> y"
echo "no -> n"
read n

if [[ $n == 'y' ]];
then
   echo "${GREEN}Cool.....se ya!!${NC}"
   exit 0

else 
   echo "${RED}Check now!${NC}"
   sudo apt install systemd-timesyncd -y
   timedatectl set-ntp true
fi