# Time Fixer For Linux

> Saikat Karmakar | Mar 2022

---

- If you're a linux user then it's nothing new for you clone the repor & run the scirpt. It also fixes time related issues for dual boot & multiboot system.

**Usage**
```bash
git clone https://gitlab.com/Aviksaikat/linux-time-fix
cd linux-time-fix
chmod +x time-fix.sh
sudo ./time-fix.sh
```